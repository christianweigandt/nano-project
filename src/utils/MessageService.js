class MessageService {

    static requestNewPayment () {
        return MessageService.postData('https://gonano.dev/payment/new', {'account': 'nano_3depebwidn63qwx6we1bsnuj56qqzzyyemqe3jk5oi1egnuqxmrxhhp1zhzx', 'amount': 0.1});
    }

    // Example POST method implementation:
    static postData(url, data) {
        // Default options are marked with *
        const fetchPromise = fetch(url, {
            method: 'POST',
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        return fetchPromise.then(
            (data) => {
                return {
                    ok: data.ok,
                    status: data.status,
                    statusText: data.statusText,
                    body: data.body
                };
            },
            (err) => {
                throw new Error('Connection failure to ' + url);
            })
    }
};

export default MessageService;