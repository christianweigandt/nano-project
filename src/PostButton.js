import React from 'react';

import './PostButton.css';
import MessageService from './utils/MessageService';

function PostButton () {

    function _handleButtonClick (e) {
        e.preventDefault();
        const promise = MessageService.requestNewPayment();
        promise.then(
            (data) => {
                alert(data.statusText);
            },
            (err) => {
                alert(err.toString());
            })
    }

    return (
        <button
            className='post-button'
            type='button'
            onClick={_handleButtonClick}
            >
            Post
        </button>
    );
}

export default PostButton;