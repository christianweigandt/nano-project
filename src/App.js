import './App.css';

import MessageDisplay from './MessageDisplay';
import PostButton from './PostButton';

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <MessageDisplay />
                <PostButton />
            </header>
        </div>
    );
}

export default App;
